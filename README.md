camel-case-selector
===================

```
npm install --save camel-case-selector
```

or

```
yarn add camel-case-selector
```

### Usage

```js
import S from 'camel-case-selector';
```

#### Generic rules and types
```js
  console.log(typeof S); //function

  console.log(document.querySelector('div') === S('div')[0]) //true

  var $div = document.querySelector('div');
  console.log(S($div) === $div); //true

  console.log(S() === document); //true

  console.log(S(document) === document); //true

  console.log(S(window) === window); //true

  console.log(S('html')); //prints array with first element html
  console.log(S('div')); //prints all divs in array
```

####Query chaining

#####To query dom element use chaining method.

```
	S parent -> query function -> [special query selector] -> ...
```

#####S parent is dom element (elements) or selector (string type) wrapper with S function

```js
  S //S parent
  S(window) //S parent
  S(document) //S parent
  S('div') // S parent

  var $div = document.querySelector('div');
  S($div) //S parent

  var $divs = document.querySelectorAll('div');
  S($divs) //S parent (parents)
```

#####Query function is properties (or methods) of S parent

```js
	S.query[special_query_selector] //query_selector is special selector. See below
    S.queryAll[special_query_selector]
    S.query(query_selector) //query_selector is simple string selector
    S.queryAll(query_selector)
```

#####Special selector is dynamic property which selects child (or childs) of parent (or parents) in DOM

######To select elements by tag name use dynamic property as tag name:
```js
	var $div = S.query.div;
	console.log($div); //first div element in DOM
    
    var $divs = S.queryAll.div;
	console.log($divs); //all divs in DOM

	var $as = S.query.nav.query.ul.queryAll.li.queryAll.a;
	console.log($as); //all a elements in navigation bar
    
    //alternatives using simple DOM selectors
    var $div = S('html').query('div');
    var $divs = S('html').queryAll('div');
    var $as = S('html nav').queryAll('ul').queryAll('li').queryAll('a');
```

######To select elements by ID use dynamic property as ID in upper camel case format:
Id's in DOM must be in upper camel case format: MainContent, Wrapper, ContentWrapper and etc.
```js
	S.query.MainContent.queryAll('div'); //selects all divs of #MainContent
    S.query.Wrapper.query.ContentWrapper.queryAll.div; //selects all divs of #ContentWrapper of #Wrapper
```

######To select elements by class names use dynamic property as class ame in camel case format:

```js
	S.queryAll.redButton; //selects all elements with class name red-button
    S.query.multipleForm.queryAll.input; //selects all input elements of first element with class name multiple-form
    
    /*if class name property is same as one of the html tag, priority is to html tag. E.g.:*/
    
    S.queryAll.form; //if DOM element has class name form (<div class="form"></div>, returns the form element (by tag name) or null if no form elements in DOM
    
    
```
####Example

Suppose we have the following html markup
```html
<div id="MainContent">
	<main>
    	<header>
        	<nav>
            	<ul>
                	<li>Item 1</li>
                    <li>Item 2</li>
                </ul>
            </nav>
        </header>
        <article>
        	<section id="FirstSection">
              <h1>Heading 1</h1>
              <p>Paragraph 1</p>
            </section>
            <section id="SecondSection" class="sample-section">
              <h2>Heading 2</h2>
              <p>Paragraph 2</p>
            </section>
        </article>
        <footer>
        	<div id="Contacts">
            	<div class="mobile-phone">xxxxxxx</div>
                <div class="address">xxxxxxx</div>
            </div>
        <footer>
    </main>
</div>
```

Queries examples

```js
	//Queries all li items in navigation bar
	S.query.MainContent.query.header.query.nav.queryAll.li;
    
    //Queries first section
    S.query.MainContent.queryAll.section[0];
    
    //Queries second section paragraph
    S.query.MainContent.query.sampleSection.query.p;
    
    //Queries mobile phone element in footer (using dynamic property)
    var mobile_phone = 'mobilePhone';
    S.query.Contacts.query[mobile_phone];
    
    //Queries address element with simple query
    S.query.MainContent.query('#Contacts .address');
```

###Author

Edvinas pranka

https://www.ceonnel.lt

# License

The MIT License (MIT)

Copyright (c) 2017 Edvinas Pranka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
