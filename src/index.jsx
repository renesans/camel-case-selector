import detect from 'is-client';

var htmlTags = [
	'a', 'abbr', 'acronym', 'address', 'applet', 'area', 'article', 'aside', 'audio', 'b', 'base', 'basefont', 'bdi', 'bdo', 'big', 'blockquote', 'body', 'br', 'button', 'canvas', 'caption', 'center', 'cite', 'code', 'col', 'colgroup', 'datalist', 'dd', 'del', 'details', 'dfn', 'dialog', 'dir', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure', 'font', 'footer', 'form', 'frame', 'frameset', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hr', 'html', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'label', 'legend', 'li', 'link', 'main', 'map', 'mark', 'menu', 'menuitem', 'meta', 'meter', 'nav', 'noframes', 'noscript', 'object', 'ol', 'optgroup', 'option', 'output', 'p', 'param', 'picture', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'section', 'select', 'small', 'source', 'span', 'strike', 'strong', 'style', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'track', 'tt', 'u', 'ul', 'var', 'video', 'wbr'
];

function toSelector(camelCase){
	if(htmlTags.includes(camelCase)) return camelCase;
	if(camelCase[0] === camelCase[0].toUpperCase()){
		return '#' + camelCase;
	}
	return '.' + camelCase.replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`);
}

var SelectorsManager = (parent = document) => {
	if(typeof parent === 'string'){
		parent = document.querySelectorAll(parent);
	}

	if(parent.length !== undefined
		&& parent !== window
		&& parent !== document){
		for(const parentItem of parent){
			parentItem.query = createProxyQuery(parentItem);
			parentItem.queryAll = createProxyQueryAll(parentItem);		
		}
	}
	parent.query = createProxyQuery(parent);
	parent.queryAll = createProxyQueryAll(parent);
	return parent;
}

var createProxyQuery = (parent = document) => {
	if(parent.length !== undefined && parent !== window && parent !== document){
		var queryFunction = (selector) => {
			if(typeof selector !== 'string') throw new Error('Selector in query("selector") must be as string type.');
			for(const parentItem of parent){
				var queryResult = parentItem.querySelector(selector);
				if(queryResult){
					queryResult.query = createProxyQuery(queryResult);
					queryResult.queryAll = createProxyQueryAll(queryResult);
					return queryResult;
				}
			}
		}

		return new Proxy(queryFunction, {
			get: function(target, name){
				for(const parentItem of parent){
					var queryResult = parentItem.query[name];
					if(queryResult){
						queryResult.query = createProxyQuery(queryResult);
						queryResult.queryAll = createProxyQueryAll(queryResult);
						return queryResult;
					}
				}
				return null;
			}
		});
	}
	var queryFunction = (selector) => {
		if(typeof selector !== 'string') throw new Error('Selector in query("selector") must be as string type.');
		if(parent === window) parent = document;
		if(parent !== document && parent.querySelector === undefined) throw new Error(`Parent is not a DOM element and not have querySelector method`);
		var resultDOM = parent.querySelector(selector);
		if(resultDOM){
			resultDOM.query = createProxyQuery(resultDOM);
			resultDOM.queryAll = createProxyQuery(resultDOM);
			return resultDOM;
		}else return resultDOM;
	}

	return new Proxy(queryFunction, {
		get: function(target, name){
			if(parent === window) parent = document;
			if(parent !== document && parent.querySelector === undefined) throw new Error(`Parent is not a DOM element and not have querySelector method`);
			var resultDOM = parent.querySelector(toSelector(name));
			if(resultDOM){
				resultDOM.query = createProxyQuery(resultDOM);
				resultDOM.queryAll = createProxyQueryAll(resultDOM);
				return resultDOM;
			}else return resultDOM;
		}
	});
}

var createProxyQueryAll = (parent = document) => {
	if(parent.length !== undefined && parent !== window && parent !== document){
		var queryFunction = (selector) => {
			if(typeof selector !== 'string') throw new Error('Selector in query("selector") must be as string type.');
			if(parent === window) parent = document;
			var results = [];
			for(const parentItem of parent){
				var queryResults = parentItem.querySelectorAll(selector);
				queryResults.forEach(item => results.push(item));
			}
			results.query = createProxyQuery(results);
			results.queryAll = createProxyQueryAll(results);
			return results;
		}

		return new Proxy(queryFunction, {
			get: function(target, name){
				let results = [];
				for(const parentItem of parent){
					results = results.concat(parentItem.queryAll[name]);
				}
				results.query = createProxyQuery(results);
				results.queryAll = createProxyQueryAll(results);
				return results;
			}
		});
	}

	var queryFunction = (selector) => {
		if(typeof selector !== 'string') throw new Error('Selector in query("selector") must be as string type.');
		if(parent === window) parent = document;
		var resultsDOM = parent.querySelectorAll(selector);
		let results = [];
		for(const singleResultDOM of resultsDOM){
			singleResultDOM.query = createProxyQuery(singleResultDOM);
			singleResultDOM.queryAll = createProxyQueryAll(singleResultDOM);
			results.push(singleResultDOM);
		}
		results.query = createProxyQuery(results);
		results.queryAll = createProxyQueryAll(results);
		return results;
	}

	return new Proxy(queryFunction, {
		get: function(target, name){
			if(parent === window) parent = document;
			if(parent !== document && parent.querySelectorAll === undefined) throw new Error(`Parent is not a DOM element and not have querySelector method`);
			var resultsDOM = parent.querySelectorAll(toSelector(name));
			let results = [];
			for(const singleResultDOM of resultsDOM){
				singleResultDOM.query = createProxyQuery(singleResultDOM);
				singleResultDOM.queryAll = createProxyQueryAll(singleResultDOM);
				results.push(singleResultDOM);
			}
			results.query = createProxyQuery(results);
			results.queryAll = createProxyQueryAll(results);
			return results;
		}
	});
}

if(detect()){
	SelectorsManager.query = createProxyQuery();
	SelectorsManager.queryAll = createProxyQueryAll();
}

export default SelectorsManager;